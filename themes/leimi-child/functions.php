<?php

/** CONNECT STYLES AND SCRIPTS START*/
require_once(__DIR__ . '/inc/styles-and-scripts.php');
/** CONNECT STYLES AND SCRIPTS END*/

/** ALLOW SVG UPLOAD START*/
require_once(__DIR__ . '/inc/addons/allow-svg.php');
/** ALLOW SVG UPLOAD END*/
