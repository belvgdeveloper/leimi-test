<?php

add_action('wp_enqueue_scripts', 'my_theme_enqueue_styles', 100);
function my_theme_enqueue_styles()
{ 
    $parenthandle = 'astra';
    $theme = wp_get_theme();
    wp_enqueue_style(
        $parenthandle,
        get_template_directory_uri() . '/style.css',
    );
    wp_enqueue_style(
        'common-style',
        get_stylesheet_directory_uri() . '/assets/build/css/main.min.css',
    );
}

function my_theme_enqueue_scripts() {   
    
    // wp_enqueue_script('owl-carousel', get_template_directory_uri() . '/assets/js/carousel/owl.carousel.min.js',array('jquery'));
    // wp_enqueue_script('html5-js', get_template_directory_uri() . '/assets/js/html5.min.js',array('jquery'));
    // wp_enqueue_script('ajax-comments', get_template_directory_uri() . '/assets/js/ajax-comments.js',array('jquery'));

    wp_enqueue_script('common-js', get_stylesheet_directory_uri() . '/assets/build/js/main.min.js',array('jquery'));


}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_scripts' );
