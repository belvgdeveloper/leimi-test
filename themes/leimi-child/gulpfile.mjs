// пути
const PATH_BUILD = './assets/build';
const PATH_BUILD_JS = 'assets/build/js/';
const PATH_BUILD_CSS = 'assets/build/css/';
const PATH_BUILD_IMG = 'assets/build/img/';
const PATH_BUILD_FONTS = 'assets/build/fonts/';


const PATH_SRC_JS = 'assets/src/js/main.js';
const PATH_SRC_CSS = 'assets/src/style/main.scss';
const PATH_SRC_IMG = 'assets/src/img/**/*.*';
const PATH_SRC_FONTS = 'assets/src/fonts/**/*.*';


const PATH_WATCH_JS = 'assets/src/js/**/*.js';
const PATH_WATCH_CSS = 'assets/src/style/**/*.scss';
const PATH_WATCH_IMG = 'assets/src/img/**/*.*';
const PATH_WATCH_FONTS = 'assets/src/fonts/**/*.*';

const PATH_CLEAN = './assets/build/*';

// Gulp
import gulp from 'gulp';
// auto server reload
import sync from 'browser-sync';
import rigger from 'gulp-rigger'; // import file to fole
import compilerSass from 'sass';
import gulpSass from 'gulp-sass'; // compile scss
import autoprefixer from 'gulp-autoprefixer'; // autoprefixer
import cleanCss from 'gulp-clean-css'; // minimize CSS
import uglify from 'gulp-uglify-es'; // minimize JavaScript
import cache from 'gulp-cache'; // cache module
import del from 'del'; // delete file and dir
import rename from 'gulp-rename';
import imagemin from 'gulp-imagemin'; // compress PNG, JPEG, GIF и SVG images
import gifsicle from 'imagemin-gifsicle';
import mozjpeg from 'imagemin-mozjpeg';
import optipng from 'imagemin-optipng';
import svgo from 'imagemin-svgo';
import notify from 'gulp-notify';

const browserSync = sync.create();
const sass = gulpSass(compilerSass);

// server start
gulp.task('browser-sync', () => {
  browserSync.init({
    server: {
      baseDir: PATH_BUILD
    },
    notify: false
  })
});


// keep стилей
gulp.task('css:build', () => {
  return gulp.src(PATH_SRC_CSS) // get main.scss
    .pipe(sass({outputStyle: 'expanded'}).on('error', notify.onError())) // scss -> css
    .pipe(autoprefixer()) // added prefixes
    .pipe(gulp.dest(PATH_BUILD_CSS))
    .pipe(rename({suffix: '.min'}))
    .pipe(cleanCss()) // minimize CSS
    .pipe(gulp.dest(PATH_BUILD_CSS))
    .pipe(browserSync.stream()) // server reload
});

// keep js
gulp.task('js:build', () => {
  return gulp.src(PATH_SRC_JS) // get the file main.js
    .pipe(rigger()) // import all files to main.js
    .pipe(gulp.dest(PATH_BUILD_JS))
    .pipe(uglify.default()) // minimize js
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest(PATH_BUILD_JS)) // move build file
    .pipe(browserSync.reload({ stream: true })) // restart server
});

// fonts
gulp.task('fonts:build', () => {
  return gulp.src(PATH_SRC_FONTS)
    .pipe(gulp.dest(PATH_BUILD_FONTS))
});

// images
gulp.task('image:build', () => {
  return gulp.src(PATH_SRC_IMG)
    .pipe(imagemin([
      gifsicle({ interlaced: true }),
      mozjpeg({ quality: 75, progressive: true }),
      optipng({ optimizationLevel: 5 }),
      svgo()
    ]))
    .pipe(gulp.dest(PATH_BUILD_IMG))
});

// delete dir build
gulp.task('clean:build', () => {
  return del(PATH_CLEAN);
});

// clear cache
gulp.task('cache:clear', () => {
  cache.clearAll();
});

// build
gulp.task('build',
  gulp.series('clean:build',
    gulp.parallel(
      'css:build',
      'js:build',
      'fonts:build',
      'image:build'
    )
  )
);

// task manager
gulp.task('watch', () => {
  gulp.watch(PATH_WATCH_CSS, gulp.series('css:build'));
  gulp.watch(PATH_WATCH_JS, gulp.series('js:build'));
  gulp.watch(PATH_WATCH_IMG, gulp.series('image:build'));
  gulp.watch(PATH_WATCH_FONTS, gulp.series('fonts:build'));
});

// task by default
gulp.task('default', gulp.series(
  'build',
  gulp.parallel('browser-sync','watch')
));
